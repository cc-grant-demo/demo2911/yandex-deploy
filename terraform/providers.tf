terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      version = "0.62.0"
    }

    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.7"
    }
  }

  backend "http" {
  }
}

provider "yandex" {
  zone = local.zone
  service_account_key_file = var.key
}

provider "kubernetes" {
  config_path = var.kubeconfig
}
locals {
  portdemodb = 6432
}

data "yandex_vpc_network" "network-demodb" {
  name = "default"
}

data "yandex_vpc_subnet" "subnet-demodb" {
  name = "default-ru-central1-a"
}

resource "yandex_vpc_security_group" "sg-demodb" {
  name       = "sg-demodb"
  network_id = data.yandex_vpc_network.network-demodb.id

  ingress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.portdemodb
    to_port        = local.portdemodb
  }

  egress {
    protocol       = "TCP"
    v4_cidr_blocks = ["0.0.0.0/0"]
    from_port      = local.portdemodb
    to_port        = local.portdemodb
  }

  labels = {
    name       = "demodb"
    managed-by = "terraform"
  }
}

resource "yandex_mdb_postgresql_cluster" "cluster-db-demodb" {
  name                = "demodb"
  environment         = "PRODUCTION"
  network_id          = data.yandex_vpc_network.network-demodb.id
  security_group_ids  = [yandex_vpc_security_group.sg-demodb.id]
  deletion_protection = false

  config {
    version = "13"
    resources {
      resource_preset_id = "s2.medium"
      disk_type_id       = "network-ssd"
      disk_size          = 20
    }
  }

  database {
    name  = "demodb"
    owner = "demouser"
  }

  user {
    name     = "demouser"
    password = "demopass"
    permission {
      database_name = "demodb"
    }
  }

  host {
    zone             = local.zone
    subnet_id        = data.yandex_vpc_subnet.subnet-demodb.id
    assign_public_ip = true
  }
}

resource "kubernetes_service" "external-service-demodb" {
  metadata {
    name      = "service-demodb"
    namespace = var.namespace
  }

  spec {
    type = "ExternalName"
    external_name = "c-${yandex_mdb_postgresql_cluster.cluster-db-demodb.id}.rw.mdb.yandexcloud.net"
  }
}